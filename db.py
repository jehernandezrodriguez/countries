import sqlite3
from sqlite3 import Error

def create_connection(file):
    conn = None
    try:
        conn = sqlite3.connect(file)
    except Error as e:
        print(e)
    return conn


def create_register(conn, data,table, fields):
    values = []
    for f in fields:
        values.append("?")
    fds = ','.join(str(x) for x in fields)
    vls = ','.join(str(x) for x in values)
    sql = "INSERT INTO "+table+" ("+fds+" ) VALUES("+vls+")"
    cur = conn.cursor()
    cur.execute(sql, data)
    return cur.lastrowid

def create_table(conn,table, definition):
    cur = conn.cursor()
    sql = 'create table if not exists ' + table + ' ('+definition+');'
    cur.execute(sql)
