import requests
class Service(object):
    def getData(self,method,url,headers):
        return requests.request(method, url, headers=headers).json()
