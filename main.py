import requests, configparser, hashlib, datetime, json,sqlite3, pandas as pd
from service import Service
from db import create_table,create_register,create_connection

regions = []
dataTable = []# datos finales
dateExp = datetime.datetime.now()
config = configparser.ConfigParser()
config.read('config.ini') # configuración de modulos

#Consumir url de regiones
print("Buscando datos ..")

headers = {'x-rapidapi-host': config['rapidapi']['host'], 'x-rapidapi-key': config['rapidapi']['key']}
service = Service()
data = service.getData("GET",config['rapidapi']['url'],headers)
#obtener regiones
for row in data:
    if row["region"] not in str(regions):
        regions.append(row["region"])

if len(regions)==0:
    print("No hay regiones.")
    exit()

#obtener paises por región

print("Calculando datos ..")

for region in regions: # enviar solicitud http para traer los paises por la region
    data = service.getData("GET",config['restcountries']['url']+region,{})
    for row in data:
        t = datetime.datetime.now() #tiempo inicial
        d = [region,row['name'],hashlib.sha1(row['name'].encode()).hexdigest() ]
        delta = (datetime.datetime.now() - t).total_seconds() * 1000 #tiempo final
        d.append(delta)
        dataTable.append(d)

# pasar los datos a un archivo json
if len(dataTable)<20000:
    with open('reports/data-'+str(dateExp)+'.json', 'w') as f:
        json.dump(dataTable, f)

df = pd.DataFrame(dataTable,columns=['Region','City Name','Languaje','Time'],dtype=float)
#imprimir DataFrame
print(df)
print("Tiempo total (ms)", df['Time'].sum())
print("Tiempo mínimo (ms)", df['Time'].min())
print("Tiempo máximo (ms)", df['Time'].max())
print("Tiempo promedio (ms)", df['Time'].mean())

# crear una conexion para guardar el resultado de los tiempos
conn = create_connection(config['db']['location'])
with conn:
    # crear tabla si no existe
    create_table(conn, config['db']['table_name'], "ident integer primary key, tiempoTotal varchar(10), tiempoPromedio varchar(10),tiempoMinimo varchar(10),tiempoMaximo varchar(10)" )
    # insertar datos en una nuava fila
    r = [df['Time'].sum(), df['Time'].mean(),df['Time'].min(),df['Time'].max()]
    r_id = create_register(conn, r,config['db']['table_name'], ["tiempoTotal","tiempoPromedio","tiempoMinimo","tiempoMaximo"] )
    print("Registro creado # ",r_id)
