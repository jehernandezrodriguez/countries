# Países por regiones

Este repositorio, muestra los datos del país por región.

## Instalación

Primero se debe instalar las dependencias con:

```
$ pip install
```
Luego, instalar sqlite3:

```
$ sudo apt install sqlite3
```

## Correr aplicación

```
$ python3 main.py

```
## Descripción de aplicación

El archivo principal es main.py. Se realizaron dos componentes: 1. Consumo de url. Se hace el envió de datos por HTTP y retorna la respuesta del servidor en formato JSON. 2. Guardar datos. Este componente, crea la tabla si no existe y tiene una función para guardar los datos de forma dinamica.

El primer proceso, que hace main.py, es traer los datos de las regiones. Luego, de que trae los datos, se agrega las regiones sin que se repita en la variable regions. Después, se recorren esas regiones y se consumen una url para traer el listado del país por región, se obtienen los datos (nombre, lenguaje) para agregarlos a la fila.

Para evitar un poco la redundancia y globalizar variables en todo el scope de aplicación, se creo un archivo llamado config.ini, en donde, se guarda los datos que se van a usar en todo el ámbito. Finalmente, se agrega los datos al dataframe y al archivo JSON.

## Autor
* **Jorge hernández**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
